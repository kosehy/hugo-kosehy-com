---
title: "Local server에 Kubeflow 1.4 설치 사전 준비 1"
date: 2021-12-16T00:15:03+09:00
# weight: 1
# aliases: ["/first"]
tags: ["kubeflow", "MLOps", "k8s"]
author: "Me"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "Desc Text."
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/kosehy/hugo-kosehy-com/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---
[k8s 설치](https://gitlab.com/kosehy/mlops-tutorial/-/blob/main/1_install_k8s.sh)

[k8s 마스터 노드 설정](https://gitlab.com/kosehy/mlops-tutorial/-/blob/main/2_copy_k8s_config.sh)
## swap off
```
# disable swap
sudo nano /etc/fstab

#/swap.img      none    swap    sw      0       0
sudo swapoff -a
```
## kubernetes 1.19.16 설치

```
# install kubernetes

sudo apt-get update && sudo apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
sudo apt-get update

# install kubelet kubeadm kubectl 1.19.16
sudo apt-get install -y kubelet=1.19.16-00 kubeadm=1.19.16-00 kubectl=1.19.16-00
# hold the .
sudo apt-mark hold kubelet kubeadm kubectl
# check kubernetes version
kubeadm version
kubelet --version
kubectl version
```
## 마스터 노드에서 kubeadm init
```
sudo kubeadm init --apiserver-advertise-address 192.168.35.10 --pod-network-cidr=192.168.0.0/24
```
## start k8s cluster
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
export KUBECONFIG=$HOME/.kube/config
```
[kubeflow에 사용할 open source 설치](https://gitlab.com/kosehy/mlops-tutorial/-/blob/main/3_install_until_kubeflow.sh)

## 칼리코 설치
```
# install calico
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
# taint node
kubectl taint nodes --all node-role.kubernetes.io/master-
```
## metallb 설치
```
# see what changes would be made, returns nonzero returncode if different
kubectl get configmap kube-proxy -n kube-system -o yaml | \
sed -e "s/strictARP: false/strictARP: true/" | \
kubectl diff -f - -n kube-system

# install metallb 0.11.0
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.11.0/manifests/namespace.yaml
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.11.0/manifests/metallb.yaml
```
## metallb_configmap.yaml
```
sudo nano metallb_configmap.yaml
```
```
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 192.168.35.100-192.168.35.110
```
# setup the metallb configmap
```
kubectl apply -f metallb_configmap.yaml
```
## kubernetes-dashboard 설치
```
# install kubernetes-dashboard
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.4.0/aio/deploy/recommended.yaml &&

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
EOF
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
EOF
```
## kubernetes-dashboard 설정
```
# edit type as LoadBalancer
KUBE_EDITOR=nano kubectl edit service -n kubernetes-dashboard kubernetes-dashboard

kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
```
## kubernetes-dashboard.configmap
```
sudo nano kubernetes-dashboard.configmap
```
```
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: REDACTED
    server: https://kubemaster:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: kubernetes-admin
  name: kubernetes-admin@kubernetes
current-context: kubernetes-admin@kubernetes
kind: Config
preferences: {}
users:
- name: kubernetes-admin
  user:
    client-certificate-data: REDACTED
    client-key-data: REDACTED
    token: PUT_YOUR_TOKEN_HERE_THAT_YOU_USED_TO_MANUALLY_LOGIN
```
## nvidia-device-plugin 0.10.0 설치
```
# install nvidia-device-plugin 0.10.0
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 \
   && chmod 700 get_helm.sh \
   && ./get_helm.sh

helm repo add nvdp https://nvidia.github.io/k8s-device-plugin \
   && helm repo update

helm install \
  --version=0.10.0 \
  --generate-name \
  nvdp/nvidia-device-plugin
```
## openebs 설치
```
# install openebs 2.11.0
# install openEBS using helm
helm repo add openebs https://openebs.github.io/charts
helm repo update

helm install openebs --namespace openebs openebs/openebs --create-namespace

# install verification
kubectl get pods -n openebs -l openebs.io/component-name=openebs-localpv-provisioner

# sleep 1 minute
sleep 30s

# change default storage class as openebs-hostpath
kubectl patch storageclass openebs-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```
## nfs-common 설치
```
sudo apt install nfs-common -y
```
## openebs-rwx 설치
```
# install openebs-rwx
kubectl apply -f https://openebs.github.io/charts/nfs-operator.yaml
```
## metric-server 0.5.2 설정
```
# install metric-server 0.5.2
sudo wget https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
```
## [metric not available issue](https://github.com/kubernetes-sigs/metrics-server/issues/300#issuecomment-525336815)
```
sudo nano components.yaml
# Add below command in delopyment containers args section
- --kubelet-insecure-tls
- --kubelet-preferred-address-types=InternalIP
```
## metric-server 0.5.2 설치
```
kubectl apply -f components.yaml
```
## Reference
