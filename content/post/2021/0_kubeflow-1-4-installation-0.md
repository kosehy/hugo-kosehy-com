---
title: "Local server에 Kubeflow 1.4 설치 사전 준비 0"
date: 2021-12-15T23:48:03+09:00
# weight: 1
# aliases: ["/first"]
tags: ["kubeflow", "MLOps", "k8s"]
author: "Me"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "Desc Text."
# canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://github.com/<path_to_repo>/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---
[설치 사전 준비](https://gitlab.com/kosehy/mlops-tutorial/-/blob/main/0_install_dl_env.sh)

## 설치 환경
```
ubuntu 20.04
Intel(R) Core(TM) i5-8500 CPU @ 3.00GHz
16GB RAM
nvidia 1060 6GB
```
## nvidia 드라이버 설치
```
# Add additional repositories
sudo add-apt-repository ppa:graphics-drivers/ppa -y

# Get the latest package lists
sudo apt-get update

# Install packages
sudo apt-get install net-tools ubuntu-drivers-common nvidia-driver-450-server nvidia-cuda-toolkit -y

# Get the latest package lists
sudo apt-get update
```
## 도커 설치
```
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" -y

# Get the latest package lists
sudo apt-get update
```
## helm3 설치
```
# Install helm3
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 \
   && chmod 700 get_helm.sh \
   && ./get_helm.sh
```
## 도커, nvidia-docker 설치
```
# install docker-ce, docker-ce-cli containerd.io
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

# Add the package repositories
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

# install nvidia-docker & nvidia-container-toolkit & openssh-client
sudo apt-get update
sudo apt-get install nvidia-docker2 -y
```
## Docker 설정
```
sudo nano /etc/docker/daemon.json

{
    "default-runtime": "nvidia",
    "exec-opts": ["native.cgroupdriver=systemd"],
    "log-driver": "json-file",
    "log-opts": {
      "max-size": "100m",
      "max-file": "5"
    },
    "storage-driver": "overlay2",
    "runtimes": {
        "nvidia": {
            "path": "nvidia-container-runtime",
            "runtimeArgs": []
        }
    }
}

sudo mkdir -p /etc/systemd/system/docker.service.d
```
## 도커, 시스템 재부팅
```
# restart the docker
systemctl restart docker

# reboot the server
reboot
```
## Reference
[NVIDIA device plugin for Kubernetes](https://github.com/NVIDIA/k8s-device-plugin#prerequisites)
