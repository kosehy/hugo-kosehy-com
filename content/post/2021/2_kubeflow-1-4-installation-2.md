---
title: "Local server에 Kubeflow 1.4 설치 및 katib 예제 실행하기"
date: 2021-12-16T00:25:03+09:00
# weight: 1
# aliases: ["/first"]
tags: ["kubeflow", "MLOps", "k8s", "katib"]
author: "Me"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "Desc Text."
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://github.com/<path_to_repo>/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---
# [kubeflow prerequistes](https://github.com/kubeflow/manifests#prerequisites)
- kubernetes 1.19 with a default StorageClass
- kustomize 3.2.0
- kubectl
## kubelet 설정
참고

[Kubelet runtime](https://gist.github.com/etheleon/80414516c7fbc7147a5718b9897b1518#kubeflow-pipelines)

[Pulling Large images](https://gist.github.com/etheleon/80414516c7fbc7147a5718b9897b1518#pulling-large-images)

```
# reconfig kubelet
KUBE_EDITOR="nano" kubectl edit configmap/kubelet-config-1.19 -n kube-system

container-runtime: docker
image-pull-progress-deadline: 10m
```
참고 [Update kube-apiserver flags](https://gist.github.com/etheleon/80414516c7fbc7147a5718b9897b1518#update-kube-apiserver-flags)
## kube-apiserver 설정
```
sudo nano /etc/kubernetes/manifests/kube-apiserver.yaml

- --service-account-issuer=kubernetes.default.svc
- --service-account-signing-key-file=/etc/kubernetes/pki/sa.key

systemctl restart kubelet
```
## kustomize 3.2.0 설치
```
# install kustomize 3.2.0
sudo wget https://github.com/kubernetes-sigs/kustomize/releases/download/v3.2.0/kustomize_3.2.0_linux_amd64
sudo chmod +x ./kustomize_3.2.0_linux_amd64
sudo mv ./kustomize_3.2.0_linux_amd64 /usr/bin/kustomize
```
## kubeflow git repository clone
```
git clone https://github.com/kubeflow/manifests.git
cd manifests
```
## kubeflow 1.4 한줄로 설치하기
```
while ! kustomize build example | kubectl apply -f -; do echo "Retrying to apply resources"; sleep 10; done
```
## Could not find CSRF cookie XSRF-TOKEN in the request.
```
# kubeflow-ingressgateway-certs.yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: kubeflow-ingressgateway-certs
  namespace: istio-system
spec:
  secretName: kubeflow-ingressgateway-certs
  issuerRef:
    kind: ClusterIssuer
    name: kubeflow-self-signing-issuer
  commonName: istio-ingressgateway.istio-system.svc
  ipAddresses:
  - 192.168.35.100
  - 192.168.35.101
```
## kubeflow-gateway
```
# KUBE_EDITOR=nano kubectl edit gateway -n kubeflow kubeflow-gateway
# add below code
tls:
      httpsRedirect: true
  - hosts:
    - '*'
    port:
      name: https
      number: 443
      protocol: HTTPS
    tls:
      credentialName: kubeflow-ingressgateway-certs
      mode: SIMPLE
```
## katib random 예제
```
# katib random-example

apiVersion: kubeflow.org/v1beta1
kind: Experiment
metadata:
  namespace: kubeflow
  name: pytorchjob-mnist
spec:
  parallelTrialCount: 3
  maxTrialCount: 12
  maxFailedTrialCount: 3
  objective:
    type: minimize
    goal: 0.001
    objectiveMetricName: loss
  algorithm:
    algorithmName: random
  parameters:
    - name: lr
      parameterType: double
      feasibleSpace:
        min: "0.01"
        max: "0.05"
    - name: momentum
      parameterType: double
      feasibleSpace:
        min: "0.5"
        max: "0.9"
  trialTemplate:
    primaryContainerName: pytorch
    trialParameters:
      - name: learningRate
        description: Learning rate for the training model
        reference: lr
      - name: momentum
        description: Momentum for the training model
        reference: momentum
    trialSpec:
      apiVersion: kubeflow.org/v1
      kind: PyTorchJob
      spec:
        pytorchReplicaSpecs:
          Master:
            replicas: 1
            restartPolicy: OnFailure
            template:
              metadata:
                annotations:
                  sidecar.istio.io/inject: "false"
              spec:
                containers:
                  - name: pytorch
                    image: docker.io/kubeflowkatib/pytorch-mnist:latest
                    command:
                      - "python3"
                      - "/opt/pytorch-mnist/mnist.py"
                      - "--epochs=1"
                      - "--lr=${trialParameters.learningRate}"
                      - "--momentum=${trialParameters.momentum}"
          <!-- Worker:
            replicas: 2
            restartPolicy: OnFailure
            template:
              spec:
                containers:
                  - name: pytorch
                    image: docker.io/kubeflowkatib/pytorch-mnist:latest
                    command:
                      - "python3"
                      - "/opt/pytorch-mnist/mnist.py"
                      - "--epochs=1"
                      - "--lr=${trialParameters.learningRate}"
                      - "--momentum=${trialParameters.momentum}" -->
```
## Open source packages Used
|Open source packages   |Version   |
|---|:---|
| kubernetes                    | 1.19.16               |
| openebs                       | 2.11.0                |
| kubeflow                      | 1.4.0                 |
| metallb                       | 0.11.0                |
| nvidia-k8s-device-plugin      | 0.11.0                |
| katib-controller              | 0.12.0                |
| kubernetes-dashboard          | 2.4.0                 |
| [dynamic-nfs-provisioner](https://github.com/openebs/dynamic-nfs-provisioner)                       | 0.11.0                |
| metrics-server                       | 0.5.2                |
| calico                       | 3.21.2                |
| Training Operator                       | 1.3.0                |
| MPI Operator                       | 0.3.0                |
| Notebook Controller                       | 1.4                |
| Tensorboard Controller                       | 1.4               |
| Central Dashboard                       | 1.4                |
| Profiles + KFAM                       | 1.4                |
| PodDefaults Webhook                       | 1.4                |
| Jupyter Web App                       | 1.4                |
| Tensorboards Web App                       | 0.11.0                |
| Volumes Web App                       | 1.4                |
| Katib                       | 0.12.0                |
| KFServing                       | 0.6.1                |
| Kubeflow Pipelines                       | 1.7                |
| Kubeflow Tekton Pipelines                       | 1.0.0                |
## Reference
[how-to-install-kubeflow1.2](https://gist.github.com/etheleon/80414516c7fbc7147a5718b9897b1518)
