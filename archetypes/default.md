---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
showToc: true
TocOpen: false
draft: true
hidemeta: false
comments: true
description: "Desc Text."
canonicalURL: "{{ .Permalink }}"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
---

